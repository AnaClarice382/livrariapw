/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.dao;

import br.com.livraria.fw.Data;
import br.com.livraria.to.TOLivro;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 71700382
 */
public class DAOLivro {
    
    // verificar todos os inserts antes de continuar
    
    public static void inserir(Connection c, TOLivro t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" insert into livro (nome, ativo) values (?, ?)");

        Data.executeUpdate(c, sql.toString(), t.getNome(), t.isAtivo());

    }

    public static void alterar(Connection c, TOLivro t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" update livro set nome = ?, ativo = ? where id = ?");

        Data.executeUpdate(c, sql.toString(), t.getNome(), t.isAtivo(), t.getId());

    }

    public static void excluir(Connection c, int id) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" delete from livro where id = ?");

        Data.executeUpdate(c, sql.toString(), id);

    }

    public static TOLivro obter(Connection c, int id) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" select id, nome, ativo from livro ");
        sql.append(" where id = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), id)) {
            if (rs.next()) {
                TOLivro t = new TOLivro();
                t.setId(rs.getInt("id"));
                t.setNome(rs.getString("nome"));
                t.setAtivo(rs.getBoolean("ativo"));
                return t;
            } else {
                return null;
            }
        }

    }

    public static List<TOLivro> lista(Connection c) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" select id, nome, ativo from livro ");
        sql.append(" order by nome ");

        List<TOLivro> l = new ArrayList<>();

        try (ResultSet rs = Data.executeQuery(c, sql.toString())) {
            while (rs.next()) {
                TOLivro t = new TOLivro();
                t.setId(rs.getInt("id"));
                t.setNome(rs.getString("nome"));
                t.setAtivo(rs.getBoolean("ativo"));
                l.add(t);
            }
        }

        return l;

    }
 
}
