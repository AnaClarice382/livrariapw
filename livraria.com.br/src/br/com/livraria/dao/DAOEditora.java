/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.dao;

import br.com.livraria.fw.Data;
import br.com.livraria.to.TOEditora;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 71700382
 */
public class DAOEditora {
    
     public static void inserir(Connection c, TOEditora t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" insert into editora (nome) values (?)");

        Data.executeUpdate(c, sql.toString(), t.getNome());

    }

    public static void alterar(Connection c, TOEditora t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" update editora set nome = ? where id = ?");

        Data.executeUpdate(c, sql.toString(), t.getNome(), t.getId());

    }

    public static void excluir(Connection c, int id) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" delete from editora where id = ?");

        Data.executeUpdate(c, sql.toString(), id);

    }

    public static TOEditora obter(Connection c, int id) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" select id, nome from editora ");
        sql.append(" where id = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), id)) {
            if (rs.next()) {
                TOEditora t = new TOEditora();
                t.setId(rs.getInt("id"));
                t.setNome(rs.getString("nome"));
                return t;
            } else {
                return null;
            }
        }

    }

    public static List<TOEditora> lista(Connection c) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" select id, nome from editora ");
        sql.append(" order by nome ");

        List<TOEditora> l = new ArrayList<>();

        try (ResultSet rs = Data.executeQuery(c, sql.toString())) {
            while (rs.next()) {
                TOEditora t = new TOEditora();
                t.setId(rs.getInt("id"));
                t.setNome(rs.getString("nome"));
                l.add(t);
            }
        }

        return l;

    }
}
