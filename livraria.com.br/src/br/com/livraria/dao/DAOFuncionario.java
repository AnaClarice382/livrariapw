/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.dao;

import br.com.livraria.fw.Data;
import br.com.livraria.to.TOFuncionario;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 71700382
 */
public class DAOFuncionario {
    
     public static void inserir(Connection c, TOFuncionario t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" insert into funcionario (nome, cpf) values (?, ?)");

        Data.executeUpdate(c, sql.toString(), t.getNome(), t.getCpf() );

    }

    public static void alterar(Connection c, TOFuncionario t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" update funcionario set nome = ?, cpf = ? where id = ?");

        Data.executeUpdate(c, sql.toString(), t.getNome(), t.getCpf(), t.getId());

    }

    public static void excluir(Connection c, int id) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" delete from funcionario where id = ?");

        Data.executeUpdate(c, sql.toString(), id);

    }

    public static TOFuncionario obter(Connection c, int id) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" select * from funcionario ");
        sql.append(" where id = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), id)) {
            if (rs.next()) {
                TOFuncionario t = new TOFuncionario();
                t.setId(rs.getInt("id"));
                t.setNome(rs.getString("nome"));
                t.setCpf(rs.getInt("cpf"));
                return t;
            } else {
                return null;
            }
        }

    }

    public static List<TOFuncionario> lista(Connection c) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" select * from funcionario ");
        sql.append(" order by nome ");

        List<TOFuncionario> l = new ArrayList<>();

        try (ResultSet rs = Data.executeQuery(c, sql.toString())) {
            while (rs.next()) {
                TOFuncionario t = new TOFuncionario();
                t.setId(rs.getInt("id"));
                t.setNome(rs.getString("nome"));
                t.setCpf(rs.getInt("cpf"));
                l.add(t);
            }
        }

        return l;

    }
}
