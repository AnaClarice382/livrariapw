/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.dao;

import br.com.livraria.fw.Data;
import br.com.livraria.to.TOLivraria;
import java.sql.Connection;
import java.sql.ResultSet;

/**
 *
 * @author 71700382
 */
public class DAOLivraria {
    
     public static void inserir(Connection c, TOLivraria t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" insert into livraria (nome, cnpj) values (?, ?) ");

        Data.executeUpdate(c, sql.toString(), t.getNome());

    }

    public static void alterar(Connection c, TOLivraria t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" update livraria set nome = ?, cnpj = ? where id = ? ");

        Data.executeUpdate(c, sql.toString(), t.getNome(), t.getId());

    }

    public static void desativar(Connection c, int id) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" update livraria set ativo = 0 ");

        Data.executeUpdate(c, sql.toString(), id);

    }

    public static TOLivraria obter(Connection c, int id) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" select * from livraria ");
        sql.append(" where id = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), id)) {
            if (rs.next()) {
                TOLivraria t = new TOLivraria();
                t.setId(rs.getInt("id"));
                t.setNome(rs.getString("nome"));
                t.setCnpj(rs.getInt("cnpj"));
                return t;
            } else {
                return null;
            }
        }

    }
}
