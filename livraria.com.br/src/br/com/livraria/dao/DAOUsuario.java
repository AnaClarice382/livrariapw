/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.dao;

import br.com.livraria.fw.Data;
import br.com.livraria.to.TOUsuario;
import java.sql.Connection;
import java.sql.ResultSet;

/**
 *
 * @author 71700382
 */
public class DAOUsuario {
     public static TOUsuario obterPorChave(Connection c, String chave) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" select id, nome, email, token, chave, expiracao from usuario ");
        sql.append(" where ");
        sql.append(" chave = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), chave)) {

            if (rs.next()) {

                TOUsuario t = new TOUsuario();
                t.setId(rs.getInt("id"));
                t.setNome(rs.getString("nome"));
                t.setEmail(rs.getString("email"));
                t.setToken(rs.getString("token"));
                t.setChave(rs.getString("chave"));
                t.setExpiracao(rs.getTimestamp("expiracao"));
                return t;

            } else {
                return null;
            }

        }

    }

    public static TOUsuario autenticacao(Connection c, TOUsuario u) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" select id, nome, email, token, chave, expiracao from usuario ");
        sql.append(" where ");
        sql.append(" email = ? ");
        sql.append(" and senha = sha1(?) "); // sha1 é uma função do mysql

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), u.getEmail(), u.getSenha())) {

            if (rs.next()) {

                TOUsuario t = new TOUsuario();
                t.setId(rs.getInt("id"));
                t.setNome(rs.getString("nome"));
                t.setEmail(rs.getString("email"));
                t.setToken(rs.getString("token"));
                t.setChave(rs.getString("chave"));
                t.setExpiracao(rs.getTimestamp("expiracao"));
                return t;

            } else {
                return null;
            }

        }

    }
    
    public static void setarToken(Connection c, TOUsuario u) throws Exception {
        
        StringBuilder sql = new StringBuilder();
        sql.append(" update usuario set token = ?, expiracao = ? where id = ? ");
        
        Data.executeUpdate(c, sql.toString(), u.getToken(), u.getExpiracao(), u.getId());
        
    }
}
