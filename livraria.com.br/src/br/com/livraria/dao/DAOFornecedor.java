/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.dao;

import br.com.livraria.fw.Data;
import br.com.livraria.to.TOFornecedor;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 71700382
 */
public class DAOFornecedor {
     public static void inserir(Connection c, TOFornecedor t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" insert into fornecedor (nome, cnpj) values (?, ?)");

        Data.executeUpdate(c, sql.toString(), t.getNome(), t.getCnpj() );

    }

    public static void alterar(Connection c, TOFornecedor t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" update fornecedor set nome = ?, cnpj = ? where id = ?");

        Data.executeUpdate(c, sql.toString(), t.getNome(), t.getCnpj(), t.getId());

    }

    public static void excluir(Connection c, int id) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" delete from fornecedor where id = ?");

        Data.executeUpdate(c, sql.toString(), id);

    }

    public static TOFornecedor obter(Connection c, int id) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" select * from fornecedor ");
        sql.append(" where id = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), id)) {
            if (rs.next()) {
                TOFornecedor t = new TOFornecedor();
                t.setId(rs.getInt("id"));
                t.setNome(rs.getString("nome"));
                t.setCnpj(rs.getInt("cnpj"));
                return t;
            } else {
                return null;
            }
        }

    }

    public static List<TOFornecedor> lista(Connection c) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" select * from fornecedor ");
        sql.append(" order by nome ");

        List<TOFornecedor> l = new ArrayList<>();

        try (ResultSet rs = Data.executeQuery(c, sql.toString())) {
            while (rs.next()) {
                TOFornecedor t = new TOFornecedor();
                t.setId(rs.getInt("id"));
                t.setNome(rs.getString("nome"));
                t.setCnpj(rs.getInt("cnpj"));
                l.add(t);
            }
        }

        return l;

    }
}
