/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.dao;

import br.com.livraria.fw.Data;
import br.com.livraria.to.TOAutor;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 71700382
 */
public class DAOAutor {
    
     public static void inserir(Connection c, TOAutor t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" insert into autor (nome) values (?)");

        Data.executeUpdate(c, sql.toString(), t.getNome());

    }

    public static void alterar(Connection c, TOAutor t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" update autor set nome = ? where id = ?");

        Data.executeUpdate(c, sql.toString(), t.getNome(), t.getId());

    }

    public static void excluir(Connection c, int id) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" delete from autor where id = ?");

        Data.executeUpdate(c, sql.toString(), id);

    }

    public static TOAutor obter(Connection c, int id) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" select id, nome from autor ");
        sql.append(" where id = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), id)) {
            if (rs.next()) {
                TOAutor t = new TOAutor();
                t.setId(rs.getInt("id"));
                t.setNome(rs.getString("nome"));
                return t;
            } else {
                return null;
            }
        }

    }

    public static List<TOAutor> lista(Connection c) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" select id, nome from autor ");
        sql.append(" order by nome ");

        List<TOAutor> l = new ArrayList<>();

        try (ResultSet rs = Data.executeQuery(c, sql.toString())) {
            while (rs.next()) {
                TOAutor t = new TOAutor();
                t.setId(rs.getInt("id"));
                t.setNome(rs.getString("nome"));
                l.add(t);
            }
        }

        return l;

    }
}
