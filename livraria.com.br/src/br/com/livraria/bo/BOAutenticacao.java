/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.bo;

import br.com.livraria.dao.DAOUsuario;
import br.com.livraria.fw.Criptografia;
import br.com.livraria.fw.Data;
import br.com.livraria.fw.DateTime;
import br.com.livraria.to.TOUsuario;
import java.sql.Connection;

/**
 *
 * @author usuario
 */
public class BOAutenticacao {
    
    
    private static final String CHAVE_PRIVADA = "COTEMIG2018";

    public static boolean autentica(String chave, String token) throws Exception {

         try (Connection c = Data.openConnection()) {

            TOUsuario t = DAOUsuario.obterPorChave(c, chave);

            if (t != null) {
                
                DateTime agora = DateTime.now();

                if (token != null && 
                        token.equals(Criptografia.sha1(t.getToken() + CHAVE_PRIVADA)) &&
                        t.getExpiracao().after(agora.getTimestamp())) {
                    return true;
                } else {
                    return false;
                }

            } else {
                return false;
            }

        }

    }
}
