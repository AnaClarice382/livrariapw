/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.bo;

import br.com.livraria.dao.DAOUsuario;
import br.com.livraria.fw.Data;
import br.com.livraria.fw.DateTime;
import br.com.livraria.fw.Guid;
import br.com.livraria.to.TOUsuario;
import java.sql.Connection;

/**
 *
 * @author 71700382
 */
public class BOUsuario {
        public static TOUsuario autenticacao(TOUsuario u) throws Exception {

        try (Connection c = Data.openConnection()) {

            TOUsuario t = DAOUsuario.autenticacao(c, u);

            if (t != null) {

                String token = Guid.getString();

                DateTime date = DateTime.now();

                date.addHour(5);

                t.setExpiracao(date.getTimestamp());
                t.setToken(token);

                DAOUsuario.setarToken(c, t);

            }

            return t;

        }

    }

}
