/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.bo;

import br.com.livraria.dao.DAOCategoriaLivro;
import br.com.livraria.fw.Data;
import br.com.livraria.to.TOCategoriaLivro;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author 71700382
 */
public class BOCategoriaLivro {
    
    public static void inserir(List<TOCategoriaLivro> l) throws Exception {
        try (Connection c = Data.openConnection()) {
            for (TOCategoriaLivro t : l) {
                DAOCategoriaLivro.inserir(c, t);
            }
        }
    }

    public static void inserir(TOCategoriaLivro t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOCategoriaLivro.inserir(c, t);
        }
    }

    public static void alterar(TOCategoriaLivro t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOCategoriaLivro.alterar(c, t);
        }
    }

    public static void excluir(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOCategoriaLivro.excluir(c, id);
        }
    }

    public static TOCategoriaLivro obter(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOCategoriaLivro.obter(c, id);
        }
    }

    public static List<TOCategoriaLivro> lista() throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOCategoriaLivro.lista(c);
        }
    }

}
