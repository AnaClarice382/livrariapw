/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.bo;

import br.com.livraria.dao.DAOLivro;
import br.com.livraria.fw.Data;
import br.com.livraria.to.TOLivro;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author 71700382
 */
public class BOLivro {
    public static void inserir(List<TOLivro> l) throws Exception {
        try (Connection c = Data.openConnection()) {
            for (TOLivro t : l) {
                DAOLivro.inserir(c, t);
            }
        }
    }

    public static void inserir(TOLivro t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOLivro.inserir(c, t);
        }
    }

    public static void alterar(TOLivro t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOLivro.alterar(c, t);
        }
    }

    public static void excluir(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOLivro.excluir(c, id);
        }
    }

    public static TOLivro obter(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOLivro.obter(c, id);
        }
    }

    public static List<TOLivro> lista() throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOLivro.lista(c);
        }
    }
}
