/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.bo;


import br.com.livraria.dao.DAOAutor;
import br.com.livraria.fw.Data;
import br.com.livraria.to.TOAutor;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author 71700382
 */
public class BOAutor {
    
    public static void inserir(List<TOAutor> l) throws Exception {
        try (Connection c = Data.openConnection()) {
            for (TOAutor t : l) {
                DAOAutor.inserir(c, t);
            }
        }
    }

    public static void inserir(TOAutor t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOAutor.inserir(c, t);
        }
    }

    public static void alterar(TOAutor t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOAutor.alterar(c, t);
        }
    }

    public static void excluir(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOAutor.excluir(c, id);
        }
    }

    public static TOAutor obter(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOAutor.obter(c, id);
        }
    }

    public static List<TOAutor> lista() throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOAutor.lista(c);
        }
    }

}
