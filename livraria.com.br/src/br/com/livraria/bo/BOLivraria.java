/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.bo;

import br.com.livraria.dao.DAOLivraria;
import br.com.livraria.dao.DAOLivraria;
import br.com.livraria.fw.Data;
import br.com.livraria.to.TOLivraria;
import br.com.livraria.to.TOLivraria;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author 71700382
 */
public class BOLivraria {
    
      public static void inserir(TOLivraria t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOLivraria.inserir(c, t);
        }
    }

    public static void alterar(TOLivraria t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOLivraria.alterar(c, t);
        }
    }

    public static void desativar(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOLivraria.desativar(c, id);
        }
    }

    public static TOLivraria obter(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOLivraria.obter(c, id);
        }
    }
}
