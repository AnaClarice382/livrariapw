/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.bo;

import br.com.livraria.dao.DAOFornecedor;
import br.com.livraria.fw.Data;
import br.com.livraria.to.TOFornecedor;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author 71700382
 */
public class BOFornecedor {
    public static void inserir(List<TOFornecedor> l) throws Exception {
        try (Connection c = Data.openConnection()) {
            for (TOFornecedor t : l) {
                DAOFornecedor.inserir(c, t);
            }
        }
    }

    public static void inserir(TOFornecedor t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOFornecedor.inserir(c, t);
        }
    }

    public static void alterar(TOFornecedor t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOFornecedor.alterar(c, t);
        }
    }

    public static void excluir(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOFornecedor.excluir(c, id);
        }
    }

    public static TOFornecedor obter(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOFornecedor.obter(c, id);
        }
    }

    public static List<TOFornecedor> lista() throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOFornecedor.lista(c);
        }
    }

}
