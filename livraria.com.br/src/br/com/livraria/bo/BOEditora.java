/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.bo;

import br.com.livraria.dao.DAOEditora;
import br.com.livraria.fw.Data;
import br.com.livraria.to.TOEditora;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author 71700382
 */
public class BOEditora {
    
     public static void inserir(List<TOEditora> l) throws Exception {
        try (Connection c = Data.openConnection()) {
            for (TOEditora t : l) {
                DAOEditora.inserir(c, t);
            }
        }
    }

    public static void inserir(TOEditora t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOEditora.inserir(c, t);
        }
    }

    public static void alterar(TOEditora t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOEditora.alterar(c, t);
        }
    }

    public static void excluir(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOEditora.excluir(c, id);
        }
    }

    public static TOEditora obter(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOEditora.obter(c, id);
        }
    }

    public static List<TOEditora> lista() throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOEditora.lista(c);
        }
    }

}
