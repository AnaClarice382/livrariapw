/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.bo;

import br.com.livraria.dao.DAOFuncionario;
import br.com.livraria.fw.Data;
import br.com.livraria.to.TOFuncionario;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author 71700382
 */
public class BOFuncionario {
    public static void inserir(List<TOFuncionario> l) throws Exception {
        try (Connection c = Data.openConnection()) {
            for (TOFuncionario t : l) {
                DAOFuncionario.inserir(c, t);
            }
        }
    }

    public static void inserir(TOFuncionario t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOFuncionario.inserir(c, t);
        }
    }

    public static void alterar(TOFuncionario t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOFuncionario.alterar(c, t);
        }
    }

    public static void excluir(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOFuncionario.excluir(c, id);
        }
    }

    public static TOFuncionario obter(int id) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOFuncionario.obter(c, id);
        }
    }

    public static List<TOFuncionario> lista() throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOFuncionario.lista(c);
        }
    }
}
