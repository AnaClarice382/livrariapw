/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.api;

import javax.ws.rs.Path;

@Path("livraria")
public class ServiceLivraria {

    @Context
    private HttpServletResponse response;

    @POST
    @Consumes("application/json; charset=utf-8")
    public void inserir(TOLivraria t) throws Exception {
        BOLivraria.inserir(t);
    }
    
    @POST
    @Path("list")
    @Consumes("application/json; charset=utf-8")
    public void inserirList(List<TOLivraria> l) throws Exception {
        BOLivraria.inserir(l);
    }

    @PUT
    @Consumes("application/json; charset=utf-8")
    public void alterar(TOLivraria t) throws Exception {
        BOLivraria.alterar(t);
    }

    @DELETE
    @Path("{id}")
    public void excluir(@PathParam("id") int id) throws Exception {
        BOLivraria.excluir(id);
    }
    
    @GET
    @Produces("application/json; charset=utf-8")
    public List<TOLivraria> lista() throws Exception {
        return BOLivraria.lista();
    }

    @GET
    @Path("{id}")
    @Produces("application/json; charset=utf-8")
    public TOLivraria obter(@PathParam("id") int id) throws Exception {
        TOLivraria t = BOLivraria.obter(id);
        
        if(t == null){
            response.sendError(404);
        }
        
        return t;
    }
}