/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.api;

import javax.ws.rs.Path;

@Path("editora")
public class ServiceEditora {

    @Context
    private HttpServletResponse response;

    @POST
    @Consumes("application/json; charset=utf-8")
    public void inserir(TOEditora t) throws Exception {
        BOEditora.inserir(t);
    }
    
    @POST
    @Path("list")
    @Consumes("application/json; charset=utf-8")
    public void inserirList(List<TOEditora> l) throws Exception {
        BOEditora.inserir(l);
    }

    @PUT
    @Consumes("application/json; charset=utf-8")
    public void alterar(TOEditora t) throws Exception {
        BOEditora.alterar(t);
    }

    @DELETE
    @Path("{id}")
    public void excluir(@PathParam("id") int id) throws Exception {
        BOEditora.excluir(id);
    }
    
    @GET
    @Produces("application/json; charset=utf-8")
    public List<TOEditora> lista() throws Exception {
        return BOEditora.lista();
    }

    @GET
    @Path("{id}")
    @Produces("application/json; charset=utf-8")
    public TOEditora obter(@PathParam("id") int id) throws Exception {
        TOEditora t = BOEditora.obter(id);
        
        if(t == null){
            response.sendError(404);
        }
        
        return t;
    }
}