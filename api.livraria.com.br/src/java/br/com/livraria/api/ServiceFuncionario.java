/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.api;

import javax.ws.rs.Path;

@Path("funcionario")
public class ServiceAutor {

    @Context
    private HttpServletResponse response;

    @POST
    @Consumes("application/json; charset=utf-8")
    public void inserir(TOAutor t) throws Exception {
        BOAutor.inserir(t);
    }
    
    @POST
    @Path("list")
    @Consumes("application/json; charset=utf-8")
    public void inserirList(List<TOAutor> l) throws Exception {
        BOAutor.inserir(l);
    }

    @PUT
    @Consumes("application/json; charset=utf-8")
    public void alterar(TOAutor t) throws Exception {
        BOAutor.alterar(t);
    }

    @DELETE
    @Path("{id}")
    public void excluir(@PathParam("id") int id) throws Exception {
        BOAutor.excluir(id);
    }
    
    @GET
    @Produces("application/json; charset=utf-8")
    public List<TOAutor> lista() throws Exception {
        return BOAutor.lista();
    }

    @GET
    @Path("{id}")
    @Produces("application/json; charset=utf-8")
    public TOAutor obter(@PathParam("id") int id) throws Exception {
        TOAutor t = BOAutor.obter(id);
        
        if(t == null){
            response.sendError(404);
        }
        
        return t;
    }
}