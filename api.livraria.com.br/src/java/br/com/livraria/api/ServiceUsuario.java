/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.api;

import javax.ws.rs.Path;

@Path("usuario")
public class ServiceUsuario {
    
    @POST
    @Path("autentica")
    @Produces("application/json; charset=utf-8")
    @Consumes("application/json; charset=utf-8")
    public TOUsuario autenticacao(TOUsuario u) throws Exception {
        return BOUsuario.autenticacao(u);
    }
    
}