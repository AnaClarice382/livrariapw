/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.api;

import javax.ws.rs.Path;

@Path("fornecedor")
public class ServiceFornecedor {

    @Context
    private HttpServletResponse response;

    @POST
    @Consumes("application/json; charset=utf-8")
    public void inserir(TOFornecedor t) throws Exception {
        BOFornecedor.inserir(t);
    }
    
    @POST
    @Path("list")
    @Consumes("application/json; charset=utf-8")
    public void inserirList(List<TOFornecedor> l) throws Exception {
        BOFornecedor.inserir(l);
    }

    @PUT
    @Consumes("application/json; charset=utf-8")
    public void alterar(TOFornecedor t) throws Exception {
        BOFornecedor.alterar(t);
    }

    @DELETE
    @Path("{id}")
    public void excluir(@PathParam("id") int id) throws Exception {
        BOFornecedor.excluir(id);
    }
    
    @GET
    @Produces("application/json; charset=utf-8")
    public List<TOFornecedor> lista() throws Exception {
        return BOFornecedor.lista();
    }

    @GET
    @Path("{id}")
    @Produces("application/json; charset=utf-8")
    public TOFornecedor obter(@PathParam("id") int id) throws Exception {
        TOFornecedor t = BOFornecedor.obter(id);
        
        if(t == null){
            response.sendError(404);
        }
        
        return t;
    }
}