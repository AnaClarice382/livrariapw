/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.livraria.api;

import javax.ws.rs.Path;

@Path("livro")
public class ServiceLivro {

    @Context
    private HttpServletResponse response;

    @POST
    @Consumes("application/json; charset=utf-8")
    public void inserir(TOLivro t) throws Exception {
        BOLivro.inserir(t);
    }
    
    @POST
    @Path("list")
    @Consumes("application/json; charset=utf-8")
    public void inserirList(List<TOLivro> l) throws Exception {
        BOLivro.inserir(l);
    }

    @PUT
    @Consumes("application/json; charset=utf-8")
    public void alterar(TOLivro t) throws Exception {
        BOLivro.alterar(t);
    }

    @DELETE
    @Path("{id}")
    public void excluir(@PathParam("id") int id) throws Exception {
        BOLivro.excluir(id);
    }
    
    @GET
    @Produces("application/json; charset=utf-8")
    public List<TOLivro> lista() throws Exception {
        return BOLivro.lista();
    }

    @GET
    @Path("{id}")
    @Produces("application/json; charset=utf-8")
    public TOLivro obter(@PathParam("id") int id) throws Exception {
        TOLivro t = BOLivro.obter(id);
        
        if(t == null){
            response.sendError(404);
        }
        
        return t;
    }
}